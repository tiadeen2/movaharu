<?php
  ini_set('include_path', '../pearlib/PEAR/' . PATH_SEPARATOR . ini_get('include_path'));
  require_once 'Mail/mimeDecode.php';
  // メールデータ取得
  $params['include_bodies'] = true;
  $params['decode_bodies']  = true;
  $params['decode_headers'] = true;
  $params['input'] = file_get_contents("php://stdin");
  $params['crlf'] = "\r\n";
  $structure = Mail_mimeDecode::decode($params);

  //送信者のメールアドレスを抽出
  $mail = $structure->headers['from'];
  $mail = addslashes($mail);
  $mail = str_replace('"','',$mail);

  //署名付きの場合の処理を追加
  preg_match("/<.*>/",$mail,$str);
  if($str[0]!=""){
    $str=substr($str[0],1,strlen($str[0])-2);
    $mail = $str;
  }
  /*
   *「$structure->headers['to']」で送信元のメールアドレスも取得できます。
   */
  $sFrom = $mail;
  
  // 件名を取得
  $diary_subject = $structure->headers['subject'];
  preg_match("/^(\w{33}):(.*)$/", $diary_subject, $aMatch);
  $sSessionID = $aMatch[1];
  $sSubject   = $aMatch[2];
  if ($sSessionID == '') {
    preg_match("/^(\w{33}):(.*)$/", $diary_subject, $aMatch);
    $sSubject   = $diary_subject;
  }
  
  switch(strtolower($structure->ctype_primary)){
    case "text": // シングルパート(テキストのみ)
      $diary_body = $structure->body;
      break;
    case "multipart":  // マルチパート(画像付き)
      foreach($structure->parts as $part){
        switch(strtolower($part->ctype_primary)){
          case "text": // テキスト
            $diary_body = $part->body;
            break;
          case "image": // 画像
            //画像の拡張子を取得する(小文字に変換
            $type = strtolower($part->ctype_secondary);
            //JPEGチェック（GIFやPNG形式の画像チェックなども可
            if($type != "jpeg" and $type != "jpg"){
              continue;
            }
            //添付内容をファイルに保存
            $filepath  = "/home/pinoxox/pinoxox.xsrv.jp/public_html/haru_mail/uploads/";
            if ($sSessionID != '') {
              $filepath .= $sSessionID .".jpg";
            } else {
srand((double)microtime()*1000000);
$number=round(rand(1000000,9999999));
              $filepath .=  $number .".jpg";
            }
            $fp = fopen( $filepath,"w" );
            $length = strlen( $part->body );
            fwrite( $fp, $part->body, $length );
            fclose( $fp );
            break;
        }
      }
    break;
    default:
    $diary_body = "";
  }
  // まだセッションIDがなければ、Bodyの最後についているハズ
  if ($sSessionID == '') {
    preg_match("/^(.*):(\w{33})$/", $diary_body, $aMatch);
    $sSessionID = $aMatch[2];
    $diary_body = $aMatch[1];
    
    $filepath_orig = $filepath;
    $filepath  = "/home/pinoxox/pinoxox.xsrv.jp/public_html/haru_mail/uploads/";
    $filepath .= $sSessionID .".jpg";
    copy($filepath_orig, $filepath);
    unlink($filepath_orig);
  }
  // ここでチェック！
  if ($sSessionID == '') {
    // 画像があったら削除
    if (is_file($filepath)) {
      unlink($filepath);
    }
    // ★エラー処理
    // スパム対策でメール返信はしない
    exit;
  }
  
  /*
   * 取得したメールアドレス、タイトル、本文、画像を使用してデータベースなどに取り込む
   */
#   $sSessionID = 'pino';
   $sSubject = urlencode(mb_convert_encoding($sSubject, 'UTF-8', 'JIS'));
   $sBody    = urlencode(mb_convert_encoding($diary_body, 'UTF-8', 'JIS'));
   $sUrl  = "http://pinoxox.xsrv.jp/haru_cgi/send_message_with_image.cgi?";
   $sUrl .= sprintf("id=%s&comment=%s/%s", $sSessionID, $sSubject, $sBody);
   
   $sResult = file_get_contents($sUrl);
   // 画像があったら削除
   if (is_file($filepath)) {
     unlink($filepath);
   }
   if ($sResult != 'SUCCESS') {
     // エラー処理
     send_error_mail($sFrom);
     send_error_mail("tiadeen2@ezweb.ne.jp", "\nsessionid:\n".$sSessionID);
     exit;
   }
   exit;

function send_error_mail($to, $msg = '') {
	$sHeader  = 'From: haru@pinoxox.xsrv.jp'."\n";
	$sSubject = mb_convert_encoding('[ﾓﾊﾞHARU MailPost]失敗しました', 'JIS');
	$sBody    = "先ほどのメール投稿に失敗しましたorz\n".
				"\n".
				"※SessionIDが切れてるかもしれないので、もう一度ログインしてみてください。\n".
				"↓ﾓﾊﾞHARU\n".
				"http://pinoxox.xsrv.jp/haru_mail/";
	$sBody    = mb_convert_encoding($sBody . $msg, 'JIS');
	mb_send_mail($to, $sSubject, $sBody, $sHeader);
}
?>