<?php
/**
 * ログインして問題なければmova_haruへリダイレクトする.
 *
 * @input  string ログインID
 * @input  string パスワード
 * @output        mova_haru/index.php へリダイレクト
 *
 */
require_once("xml2json/xml2json.php");

// ログイン
$sLoginId = $_POST['id'];
$sPasswd  = $_POST['pw'];
$sSessionid = login($sLoginId, $sPasswd);
if ($sSessionid == null) {
	header('Location: http://pinoxox.xsrv.jp/haru_mail/mova_haru_post/setting.php?err=1');
	exit;
}

// HaruIDを取得
$iHaruId = getHaruId($sLoginId);
if ($iHaruId == null) {
	header('Location: http://pinoxox.xsrv.jp/haru_mail/mova_haru_post/setting.php?err=1');
	exit;
}

// mova_haruへリダイレクト
header('Location: http://pinoxox.xsrv.jp/haru_mail/mova_haru_post/index.php?haruid='. $iHaruId .'&sessionid='. $sSessionid);
exit;


function login($sLoginId, $sPasswd) {
	$sURL = "http://pinoxox.xsrv.jp/"."haru_cgi/get_session.cgi";
	$aPostdata = array('id' => $sLoginId,'pw' => $sPasswd);
	$sPostdata = http_build_query($aPostdata);
	$aParams = array('http' =>
						array(
							'method' => 'POST',
							'header' => 'Content-type: application/x-www-form-urlencoded',
							'content' => $sPostdata
						)
					);
	$rContext = stream_context_create($aParams);
	$sResult = file_get_contents($sURL, false, $rContext);
	if (preg_match('/^login success:/', $sResult) == false)
		return null;
	
	// セッションIDを返す
	$sSessionid = preg_replace('/^(login success:)/', '', $sResult);
	return $sSessionid;
}

function getHaruId($sLoginId) {
	$sURL = 'http://haru.fm/api?command=userno&loginid='. $sLoginId;
	$sTargetXML = file_get_contents($sURL);
	
	$oSimpleXmlElementObject = simplexml_load_string($sTargetXML);	
	if ($oSimpleXmlElementObject == null) {
		return null;
	}
	$aJson  = xml2json::convertSimpleXmlElementObjectIntoArray($oSimpleXmlElementObject);
	return $aJson['response']['user']['id'];
}
