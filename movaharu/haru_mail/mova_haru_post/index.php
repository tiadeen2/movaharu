<?php
set_include_path('libs/smarty:'.get_include_path());
require_once "Smarty.class.php";


// ログイン
$iHaruId     = $_GET['haruid'];
$sSessionid  = $_GET['sessionid'];
if ($iHaruId == '' or $sSessionid == '') {
	header('Location: http://pinoxox.xsrv.jp/haru_mail/mova_haru_post/setting.php?err=2');
	exit;
}

// 出力
$oSmarty = new Smarty();
$oSmarty->assign("iHaruId",       $iHaruId);
$oSmarty->assign("sSessionid",    $sSessionid);
$sOutput = $oSmarty->fetch('mova_haru_post.tpl');

//mb_http_output("Shift_JIS");
echo mb_convert_encoding($sOutput, "UTF-8", "EUC-JP");
exit;
