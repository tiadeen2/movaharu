#! /usr/bin/perl -w

BEGIN {
	unshift(@INC, './libs/');
}
use strict;
use WWW::Mechanize;
use CGI;

# 定数
my $COOKIE_DIR = 'cookies/';
my $POST_URL   = 'http://haru.fm/user/';
my $IMAGE_DIR  = '../haru_mail/uploads/';


##########################################################################

# パラメータ取得
my $oCGI = new CGI();
my $sID = $oCGI->param('id');
my $sComment = $oCGI->param('comment');

# セッションを読み込む
use HTTP::Cookies;
my $cookie_jar = HTTP::Cookies->new(ignore_discard => 1);
$cookie_jar->load($COOKIE_DIR .'/'. $sID .'.txt');

# コメントフォームへ遷移
my $oMech = post($POST_URL, $sID, $sComment, $cookie_jar);
if ($oMech eq 'faild') {
	print "Content-type:text/plain\n\n";
	print "ERROR";
	exit;
}

# 成功
print "Content-type:text/plain\n\n";
print "SUCCESS";
exit;

##########################################################################
sub post {
	my $sUrl = shift;
	my $sID  = shift;
	my $sComment   = shift;
	my $cookie_jar = shift;
	my $sImagePath = $IMAGE_DIR . $sID .'.jpg';
	
	my $oMech = WWW::Mechanize->new(cookie_jar => $cookie_jar);
	$oMech->agent('Mozilla/5.0');
	$oMech->get($sUrl);
	
	$oMech->form_number(1);
	$oMech->field('rakugaki', $sComment);
	$oMech->field('embedImage', $sImagePath) if (-f $sImagePath);
	$oMech->submit();
	
	return 'faild' unless ($oMech->content() =~ /<title>reload<\/title>/);
	return $oMech;
}
