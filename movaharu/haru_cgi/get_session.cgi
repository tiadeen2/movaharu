#! /usr/bin/perl -w

BEGIN {
	unshift(@INC, './libs/');
}
use strict;
use WWW::Mechanize;
use CGI;

# 定数
my $COOKIE_DIR = 'cookies/';
my $LOGIN_URL  = 'http://haru.fm/login';


##########################################################################
print "Content-type: text/html\n\n";

# パラメータ取得
my $oCGI = new CGI();
my $sID = $oCGI->param('id');
my $sPW = $oCGI->param('pw');

# バリデーション
if ($sID =~ /\.\./) {
	print "login faild.";
	exit;
}

# ログインする
my $oMech = login($LOGIN_URL, $sID, $sPW);
if ($oMech eq 'faild') {
	print "login faild.";
	exit;
}
# セッションはちゃんと取れた?
if ($oMech->cookie_jar->as_string() !~ /haru_member_id=/) {
	print "login faild.";
	exit;
}

# セッションを保存する
my $sSessionFilename = make_cookie_filename();
$oMech->cookie_jar->save($COOKIE_DIR .'/'. $sSessionFilename .'.txt');

# 成功
print "login success:". $sSessionFilename;
exit;

##########################################################################
sub login {
	my $sUrl = shift;
	my $sID  = shift;
	my $sPW  = shift;
	
	my $oMech = WWW::Mechanize->new(cookie_jar => {ignore_discard => 1});
	$oMech->agent('Mozilla/5.0');
	$oMech->get($sUrl);

	$oMech->form_number(1);
	$oMech->field('choixerid', $sID);
	$oMech->field('pwd',       $sPW);
	$oMech->field('phase',     '1');
	$oMech->tick(rememberid => 'checkbox');
	$oMech->submit();
	
	return 'faild' unless ($oMech->content() =~ /ようこそ/);
	return $oMech;
}

sub make_cookie_filename {
	my @alpha = ('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
				 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
				 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0);
	
	srand();
	my $str = '';
	my $num = @alpha - 1;
	for (my $i = 0 ; $i < 33 ; $i++) {
		$str .= $alpha[int(rand($num))];
	}
	return $str;
}