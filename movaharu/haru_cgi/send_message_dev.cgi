#! /usr/bin/perl -w

BEGIN {
	unshift(@INC, './libs/');
}
use strict;
use WWW::Mechanize;
use CGI;

# 定数
my $COOKIE_DIR = 'cookies';
my $COMMENT_URL = 'http://haru.fm/post/';
my $COMMENTED_DIR = 'comments';


##########################################################################

# パラメータ取得
my $oCGI = new CGI();
my $sID      = $oCGI->param('id');
my $iLength  = $oCGI->param('length');

# セッションを読み込む
use HTTP::Cookies;
my $cookie_jar = HTTP::Cookies->new(ignore_discard => 1);
$cookie_jar->load($COOKIE_DIR .'/'. $sID .'.txt');

my $bError = "false";
my @aSuccess = [];
my $oSuccessMech = "";
for (my $i = 0 ; $i < $iLength ; $i++ ) {
	my $iNo        = $oCGI->param('no'.$i);
	my $sComment   = $oCGI->param('comment'.$i);
	my $iCommentId = $oCGI->param('cmtid'.$i);
	
	# コメントしていいかどうかのチェック
	my $sResult = checkCommentedNo($sID, $iNo, $iCommentId);
	next if ($sResult eq "same");
	
	# コメントフォームへ遷移
	my $oMech = comment($COMMENT_URL . $iNo, $sComment, $cookie_jar);
	if ($oMech eq 'faild') {
		$bError = "true";
		next;
	}
	
	# コメントNoを保存
	saveCommentedNo($sID, $iNo, $iCommentId);
	
	# コメントできたものの保存 (Flashに返す)
	push(@aSuccess, sprintf("%d:%d", $iNo, $iCommentId));
	
	# 成功したMechをください
	$oSuccessMech = $oMech;
}

# セッションを保存
my $sResult = save_cookie($oSuccessMech, $sID);
$bError = 'true' if ($sResult eq 'faild');

# 成功した?
if ($bError eq "true") {
	print "Content-type:text/plain\n\n";
	print "res=ERROR";
	exit;
}

# 成功
print "Content-type:text/plain\n\n";
print "res=SUCCESS";
#  print "&". join(",",$aSuccess);
exit;

##########################################################################
sub comment {
	my $sUrl = shift;
	my $sComment   = shift;
	my $cookie_jar = shift;
	
	my $oMech = WWW::Mechanize->new(cookie_jar => $cookie_jar);
	$oMech->agent('Mozilla/5.0');
	$oMech->get($sUrl);
	
	$oMech->form_number(1);
	$oMech->field('rakugaki', $sComment);
	$oMech->submit();
	
	return 'faild' unless ($oMech->content() =~ /<title>reload<\/title>/);
	return $oMech;
}

sub saveCommentedNo {
	my $sID = shift;
	my $iNo = shift; # 実はスレIDは使わない同じスレに何度もコメるかもしれないからね
	my $iCommentId = shift;
	my $sCommentedFilename = $COMMENTED_DIR .'/'. $sID .'.txt';
	
	open(WRITE, '>>'.$sCommentedFilename);
	print WRITE $iCommentId ."\n";
}

sub checkCommentedNo {
	my $sID = shift;
	my $iNo = shift;
	my $iCommentId = shift;
	my $sCommentedFilename = $COMMENTED_DIR .'/'. $sID .'.txt';
	
	return "notFound" unless -e $sCommentedFilename;
	
	my $iFileAccessTime = ((stat $sCommentedFilename)[9]) + 10*60;
	if (time() > $iFileAccessTime) {
		unlink($sCommentedFilename);
		return "timeover";
	}
	
	open(READ, $sCommentedFilename);
	while (<READ>) {
		chomp($_);
		if ($_ == $iCommentId) {
			return "same";
		}
	}
	return "fine";
}

sub save_cookie {
	my $oMech = shift;
	my $sSessionFilename = shift;
	
	# セッションはちゃんと取れた?
	if ($oMech->cookie_jar->as_string() !~ /haru_member_id=/) {
		return "faild";
	}

	# セッションを保存する
	$oMech->cookie_jar->save($COOKIE_DIR .'/'. $sSessionFilename .'.txt');
	return "success";
}