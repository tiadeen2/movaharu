<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ﾓﾊﾞHARU - ｱﾙﾌｧ</title>
<link rel="stylesheet" href="../css/style.css" type="text/css" />
</head>
<body bgcolor="#ffffff">
<p>ﾓﾊﾞ HARU - ｱﾙﾌｧ</p>
<br />
設定完了です。このﾍﾟｰｼﾞをﾌﾞｯｸﾏｰｸして使ってください。<br />
<br />
<div class="contents">
<span class="caption">■ﾓﾊﾞHARU</span><br />
<a href="{$mova_haru_swf}?haruid={$iHaruId}&sessionid={$sSessionid}">【起動ﾎﾞﾀﾝ】</a><br />
<span class="caution">※ｱｸｾｽ先がない場合はこのﾍﾟｰｼﾞを再表示してください。</span><br />
</div>

<div class="contents">
<span class="caption">■ﾓﾊﾞHARU MailPost(実験中) -本文ﾊﾞｰｼﾞｮﾝ</span><br />
<a href="mailto:haru@pinoxox.xsrv.jp?body=:{$sSessionid}">【ﾒｰﾙで投稿】</a><br />
<span class="caution">※『本文』に33文字のSessionIDが入りますが、その 前 に入力してください。</span><br />
<span class="caution">※現在画像はjpg形式しか受け付けてません。問題があれば相談してください (by <a href="http://haru.fm/user/6552">ﾓﾊﾞHARUｯ子</a>)</span><br />
<br />
いちいちブラウザを表示するのが面倒なようであれば、↓こちらがあなたのSessionIDですので、『本文』の前につけてﾒｰﾙ送信してください。<br />
<form>
<input type="text" value=":{$sSessionid}" />
</form>
</div>

<div class="contents">
<span class="caption">■使い方</span><br />
各画面のｷｰ操作について、画面ｷｬﾌﾟﾁｬといっしょに↓こちらにまとめました。<br />
<a href="../usage.html">◎ﾓﾊﾞHARUの使い方</a>
</div>

<div class="contents">
<span class="caption">■更新履歴</span>
<li>2008/01/16:<br />複数のｺﾒﾝﾄを一度に送れるようにしました〜</li>
<li>2007/11/28:<br />ｻｰﾊﾞを移設しました〜 (xserverというところに引越しました☆)</li>
<li>2007/11/19:<br />画像表示部分をViewer風にしました〜</li>
<li>2007/10/20:<br />書き込みにｺﾒﾝﾄできるようにしました〜</li>
<li>2007/09/15:<br />初期ﾊﾞｰｼﾞｮﾝﾘﾘｰｽ</li>
</ul>
</div>

<div class="contents">
<span class="caption">■不具合/お問い合わせ</span><br />
<a href="http://haru.fm/user/6552">HARU.FM</a>内のﾓﾊﾞHARUｯ子へ「＃ﾓﾊﾞHARUｯ子＃ 内容」でください。
</div>

<a href="../index.html">ﾄｯﾌﾟに戻る</a>

</body>
</html>
