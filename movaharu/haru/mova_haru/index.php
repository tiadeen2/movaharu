<?php
set_include_path('libs/smarty:'.get_include_path());
require_once "Smarty.class.php";

// バージョン変えるときがあると思うんで、こっからどうじょ
define('MOVA_HARU_SWF', '080116v2350mova_haru.swf');


// ログイン
$iHaruId     = $_GET['haruid'];
$sSessionid  = $_GET['sessionid'];
if ($iHaruId == '' or $sSessionid == '') {
	header('Location: http://pinoxox.xsrv.jp/haru/mova_haru/setting.php?err=2');
	exit;
}

// 出力
$oSmarty = new Smarty();
$oSmarty->assign("mova_haru_swf", MOVA_HARU_SWF);
$oSmarty->assign("iHaruId",       $iHaruId);
$oSmarty->assign("sSessionid",    $sSessionid);
$sOutput = $oSmarty->fetch('mova_haru.tpl');

//mb_http_output("Shift_JIS");
echo mb_convert_encoding($sOutput, "UTF-8", "EUC-JP");
exit;
