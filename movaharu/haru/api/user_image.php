<?php
/**
 * haruidに対応する画像を取得して、そのまま返す.
 *
 * @input  number GETでharuidを指定する
 * @output binary 画像(gif) 24x24
 *
 */

$iHaruId   = $_GET['haruid'];
// validation
// ...

$sImageURL  = 'http://haru.fm/userimages/'.$iHaruId.'/small/';

$aImageInfo = getimagesize($sImageURL);

mb_http_output("pass");
header("Content-type:". $aImageInfo['mime']);
//header("Content-length:". filesize($sImageURL));

readfile($sImageURL);