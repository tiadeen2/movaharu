<?php
/**
 * 単に okra.ark-web.jp へパラメータを送りたいので、そのプロキシコード.
 *
 */

// au はコンテントタイプを明示的に指示する必要アリ
header("Content-type:text/plain");

$sParam = '';
for ($i = 0 ; $i < $_GET['length'] ; $i++) {
	$sComment = mb_detect_encoding($_GET['comment'.$i]) == 'UTF-8' ? $_GET['comment'.$i] : mb_convert_encoding($_GET['comment'.$i], 'UTF-8');
	$sParam .= sprintf("&no%s=%s", $i, $_GET['no'.$i]);
	$sParam .= sprintf("&comment%s=%s", $i, urlencode($sComment));
	$sParam .= sprintf("&cmtid%s=%s", $i, $_GET['cmtid'.$i]);
}

$sURL = "http://pinoxox.xsrv.jp/"."haru_cgi/send_message.cgi?id=". $_GET['id'] ."&length=". $_GET['length'] . $sParam;
echo file_get_contents($sURL);
/*
	$sURL = "http://okra.ark-web.jp/~takemura/cgi-bin/public/"."haru/get_session.cgi";
	$aPostdata = array('id' => $sLoginId,'pw' => $sPasswd);
	$sPostdata = http_build_query($aPostdata);
	$aParams = array('http' =>
						array(
							'method' => 'POST',
							'header' => 'Content-type: application/x-www-form-urlencoded',
							'content' => $sPostdata
						)
					);
	$rContext = stream_context_create($aParams);
	$sResult = file_get_contents($sURL, false, $rContext);
*/