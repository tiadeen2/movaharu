<?php
/**
 * HaruIDに対応するXMLを取得して、FlashのloadVariablesで受け取れる形に変換して返す.
 *
 * @input  number GETでHaruIDを指定する
 * @output string res=結果 SUCCESS/ERROR
 *
 * 出力形式：
 *   
 */
require_once("xml2json/xml2json.php");

// au はコンテントタイプを明示的に指示する必要アリ
header("Content-type:text/plain");


$sMode   = $_GET['mode'];
$iHaruId = $_GET['haruid'];
$iPage   = $_GET['page'];
if ($sMode == 'friends') {
	// 友だちとの書き込み
	$sURL = 'http://haru.fm/api?command=userNfriend&userid='. $iHaruId .'&page='. $iPage;
} else if ($sMode == 'self') {
	// 自分の書き込み
	$sURL = 'http://haru.fm/api?command=userpost&userid='. $iHaruId .'&page='. $iPage;
} else {
	// ラウンジ
	$sURL = 'http://haru.fm/api?page='. $iPage;
}

$sTargetXML = file_get_contents($sURL);
//$sTargetXML = file_get_contents('api.xml');

$oSimpleXmlElementObject = simplexml_load_string($sTargetXML);	
if ($oSimpleXmlElementObject == null) {
	echo "res=ERROR";
	exit;
}
$aJson  = xml2json::convertSimpleXmlElementObjectIntoArray($oSimpleXmlElementObject);
$aItems = $aJson['response']['list']['item'];
if (is_array($aItems) == false) {
	echo "res=ERROR";
	exit;
}

echo "res=SUCCESS&max_list=".count($aItems);
foreach ($aItems as $iNo => $aItem) {
	echo "&item".$iNo."_id="           . urlencode($aItem['id']);
	echo "&item".$iNo."_content="      . wavedash2fullwidthtilde(urlencode(str2htmlReplace($aItem['content'])));
	echo "&item".$iNo."_regDate="      . urlencode(convRegDate($aItem['regDate']));
	echo "&item".$iNo."_haruid="       . urlencode($aItem['user']['id']);
	echo "&item".$iNo."_user="         . urlencode($aItem['user']['displayName']);
	echo "&item".$iNo."_commentCount=" . urlencode($aItem['commentCount']);
	if (array_key_exists('tags', $aItem)) {
		$iTagCount = calcTagCount($aItem['tags']);
		echo "&item".$iNo."_tagCount=". $iTagCount;
		if ($iTagCount == 1) {
			// ココ、コード二重化しちゃった:p
			$iTagNo = 0;
			$aTag   = $aItem['tags']['tag'];
			echo "&item".$iNo."_tag".$iTagNo."_name=". urlencode($aTag['name']);
		} else {
			foreach ($aItem['tags']['tag'] as $iTagNo => $aTag) {
				echo "&item".$iNo."_tag".$iTagNo."_name=". urlencode($aTag['name']);
			}
		}
	} else {
		echo "&item".$iNo."_tagCount=0";
	}
	if (array_key_exists('comments', $aItem)) {
		if ($aItem['commentCount'] == 1) {
			// ココ、コード二重化しちゃった:p
			$iCommentNo = 0;
			$aComment   = $aItem['comments']['comment'];
			echo "&item".$iNo."_comment".$iCommentNo."_content=". wavedash2fullwidthtilde(urlencode(str2htmlReplace($aComment['content'])));
			echo "&item".$iNo."_comment".$iCommentNo."_regDate=". urlencode(convRegDate($aComment['regDate']));
			echo "&item".$iNo."_comment".$iCommentNo."_user="   . urlencode($aComment['user']['displayName']);
		} else {
			foreach ($aItem['comments']['comment'] as $iCommentNo => $aComment) {
				echo "&item".$iNo."_comment".$iCommentNo."_content=". wavedash2fullwidthtilde(urlencode(str2htmlReplace($aComment['content'])));
				echo "&item".$iNo."_comment".$iCommentNo."_regDate=". urlencode(convRegDate($aComment['regDate']));
				echo "&item".$iNo."_comment".$iCommentNo."_user="   . urlencode($aComment['user']['displayName']);
			}
		}
	}
	echo "&item".$iNo."_image=" . urlencode(convImage($aItem['images']['image']['uri']));
}


function str2htmlReplace($sStr) {
	$sStr = htmlspecialchars($sStr);
	$aStr = explode('\n', $sStr);
	return join('<br>', $aStr);
}
function wavedash2fullwidthtilde($sUrlEncodedStr) {
	return preg_replace("/%E3%80%9C/", "%EF%BD%9E", $sUrlEncodedStr);
}
function calcTagCount($aTags) {
	// どうも配列が1つしかない場合は、idが直で入ってたりしているので、その対応
	if (array_key_exists('id', $aTags['tag'])) {
		return 1;
	}
	return count($aTags['tag']);
}

function convRegDate($sStr) {
	return substr($sStr, 0,19);
}

function convImage($sStr) {
	return preg_replace("/^(\/postimage\/)/", '', $sStr);
}