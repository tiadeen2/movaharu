<?php
/**
 * imageIDに対応する画像を取得して、そのまま返す.
 *
 * @input  number GETでimageIDを指定する
 * @output binary 画像(JPG/GIF)
 *
 */

$iWidth  = 235;
$iHeight = 270;
if ($_GET['mode'] == 'export') {
	$iWidth  = 240;
	$iHeight = 320;
}
else if ($_GET['mode'] == 'small') {
	$iWidth  = 156;
	$iHeight = 180;
}
define('MAX_WIDTH',  $iWidth);
define('MAX_HEIGHT', $iHeight);

$iImageId = $_GET['imageid'];
// validation
// ...


$sImageURL  = 'http://haru.fm/postimage/'. $iImageId;

$aImageInfo = getimagesize($sImageURL);
$iWidth  = $aImageInfo[0];
$iHeight = $aImageInfo[1];

mb_http_output("pass");

// jpeg でも gif でも png でもなければエラー
if ($aImageInfo['mime'] != 'image/jpeg' and $aImageInfo['mime'] != 'image/gif' and $aImageInfo['mime'] != 'image/png') {
	returnErrorImage();
	exit;
}

// サイズに合わせてリサイズして返す (リサイズの必要がなければそのまま返す)
$iWidthSub  = MAX_WIDTH  - $iWidth;
$iHeightSub = MAX_HEIGHT - $iHeight;
if ($iWidthSub < 0 or $iHeightSub < 0) {
	$rOrigImage = getOrigImage($sImageURL, $aImageInfo['mime']);
	if (($iWidthSub - $iHeightSub) < 0) {
		// 横長の対応
		$rOrigImage = imagerotate($rOrigImage, 270, 0); // 横だけ-90度回転
		$iTemp   = $iWidth;
		$iWidth  = $iHeight;
		$iHeight = $iTemp;
	} else {
		// 縦長の対応
	}
	$iNewHeight   = floor(MAX_WIDTH * $iHeight / $iWidth);
	$rNewImage = resize($rOrigImage, $aImageInfo['mime'], $iWidth, $iHeight, MAX_WIDTH, $iNewHeight);
	display($rNewImage, $aImageInfo['mime']);
} else {
	header("Content-type:". $aImageInfo['mime']);
	readfile($sImageURL);
}

function getOrigImage($sImageURL, $sMime) {
	if ($sMime == 'image/jpeg') {
		return imagecreatefromjpeg($sImageURL);
	} else if ($sMime == 'image/gif') {
		return imagecreatefromgif($sImageURL);
	} else if ($sMime == 'image/png') {
		return imagecreatefrompng($sImageURL);
	}
	return null;
}
function resize($rOrigImage, $sMime, $iOrigWidth, $iOrigHeight, $iNewWidth, $iNewHeight) {
	$rNewImage = @imagecreatetruecolor($iNewWidth, $iNewHeight);
	imagecopyresized($rNewImage, $rOrigImage, 0, 0, 0, 0, $iNewWidth, $iNewHeight, $iOrigWidth, $iOrigHeight);
	imagedestroy($rOrigImage);
	return $rNewImage;
}

function display($rNewImage, $sMime) {
	header("Content-type:". $sMime);
	if ($sMime == 'image/jpeg') {
		imagejpeg($rNewImage, null, 100);
	} else if ($sMime == 'image/gif') {
		imagegif($rNewImage);
	} else if ($sMime == 'image/png') {
//		imagepng($rNewImage);
		imagejpeg($rNewImage, null, 100);
	}
	imagedestroy($rNewImage);
}

function returnErrorImage() {
	$rNewImage = @imagecreatetruecolor(100, 50);
	$bg = imagecolorallocate($rNewImage, 255, 255, 255);
	$iTextcolor = imagecolorallocate($rNewImage, 0, 0, 255);
	imagestring($rNewImage, 2, 20, 5, "CAN NOT READ IMAGE...", $iTextcolor);
	header("Content-type: image/gif");
	imagegif($rNewImage);
	imagedestroy($rNewImage);
}