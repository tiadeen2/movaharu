﻿class DetailItem {
	private var _oData_mc:MovieClip;
	private var _sImage:String;
	private var _sCondition:String;
	private var _iImageWidth:Number;
	private var _iImageHeight:Number;

	/**
	 * コンストラクタ
	 */
	public function DetailItem(_oData_mc:MovieClip) {
		this._oData_mc = _oData_mc;
		this._sImage   = '';
		this._sCondition = 'detail';
		this._iImageWidth  = null;
		this._iImageHeight = null;
	}
	
	/**
	 * 友だちとの書き込み詳細表示
	 */
	public function makeDetailPost(iNo:Number) {
		this._sCondition = 'detail';
		var oItemBox_mc:MovieClip = _root.detailBox_mc;
		var aContent = {content:'',regDate:'',user:'',haruid:'',commentCount:0,tags:'',comment:'',image:''};
		aContent['content']      = eval('this._oData_mc.item'+iNo+'_content');
		aContent['regDate']      = eval('this._oData_mc.item'+iNo+'_regDate');
		aContent['user']         = eval('this._oData_mc.item'+iNo+'_user');
		aContent['haruid']       = eval('this._oData_mc.item'+iNo+'_haruid');
		aContent['commentCount'] = eval('this._oData_mc.item'+iNo+'_commentCount');
		aContent['tagCount']     = eval('this._oData_mc.item'+iNo+'_tagCount');
		if (aContent['tagCount'] > 0) {
			var aTags = new Array();
			for (var k:Number = 0 ; k < aContent['tagCount'] ; k++) {
				aTags.push(eval('this._oData_mc.item'+iNo+'_tag'+k+'_name'));
			}
			aContent['tags'] = '【' + aTags.join('】【') + '】';
		}
		if (aContent['commentCount'] > 0) {
			var aComments = new Array();
			for (var k:Number = 0 ; k < aContent['commentCount'] ; k++) {
				var oComment = {content:'',regDate:'',user:''};
				oComment['content'] = eval('this._oData_mc.item'+iNo+'_comment'+k+'_content');
				oComment['content'] = this._convHTMLDecode(oComment['content']);
				oComment['regDate'] = eval('this._oData_mc.item'+iNo+'_comment'+k+'_regDate');
				oComment['user']    = eval('this._oData_mc.item'+iNo+'_comment'+k+'_user');
				aComments.push(oComment['content']+' by <font color="#0000ff">'+oComment['user']+'</font> '+oComment['regDate']);
			}
			aContent['comment'] = aComments.join('<br>*━━━━━━━━━━━━*<br>');
			aContent['comment']+= '<br>*━━━━━━━━━━━━*<br>'
		}
		aContent['image']       = eval('this._oData_mc.item'+iNo+'_image');
		if (aContent['image'] != '') {
			this._sImage = aContent['image'];
		} else {
			this._sImage = '';
		}
/*
		// 分けた場合
		oItemBox_mc.content_txt.htmlText = '<font color="#0000ff">'+aContent['user']+'</font>'+'：'+aContent['content'];
		oItemBox_mc.time_txt.htmlText    = aContent['tags']+' at '+aContent['regDate']+' コメ数:'+aContent['commentCount'];
		oItemBox_mc.comment_txt.htmlText = aContent['comment'];
//		trace(oItemBox_mc.comment_txt.maxscroll);
		if (oItemBox_mc.comment_txt.maxscroll > 1) _root.scroll_txt.text = '２：▲/▼：５';
		else                                       _root.scroll_txt.text = '';
*/
		// 一緒にしちゃう
		oItemBox_mc.content_txt.htmlText  = '<font color="#0000ff">'+aContent['user']+'</font>'+'：' + aContent['content'];
		oItemBox_mc.content_txt.htmlText += '<br>';
		oItemBox_mc.content_txt.htmlText += aContent['tags']+' at '+aContent['regDate']+' コメ数:'+aContent['commentCount'];
		oItemBox_mc.content_txt.htmlText += aContent['image']!='' ? '[画像アリ:６]' : '';
		oItemBox_mc.content_txt.htmlText += '<br>*━━━━━━━━━━━━*<br>';
		oItemBox_mc.content_txt.htmlText += aContent['comment'];
//		oItemBox_mc.userImage_mc.loadMovie(_global.oMain.getBaseURL()+'api/user_image.php?haruid='+aContent['haruid']); // ただし、ユーザクリックじゃないと出ないぞ！
		if (oItemBox_mc.content_txt.maxscroll > 1) _root.scroll_txt.text = '２：▲/▼：５';
		else                                       _root.scroll_txt.text = '';
		oItemBox_mc.content_txt.scroll = 1;
		
		if (this._sImage != '') _root.image_txt.text = '画像：６';
		else                    _root.image_txt.text = '';
	}
	private function _convHTMLDecode(sTarget:String):String {
		var aStrList:Array = new Array();
		aStrList = sTarget.split('&amp;');
		sTarget  = aStrList.join('&');
//		aStrList = sTarget.split('&lt;');
//		sTarget  = aStrList.join('<');
//		aStrList = sTarget.split('&gt;');
//		sTarget  = aStrList.join('>');
//		aStrList = sTarget.split('&dquot;');
//		sTarget  = aStrList.join('"');
		return sTarget;
	}
	
	public function hasImage():Boolean {
		return this._sImage ? true : false;
	}
	
	public function makeImage(sSize:String) {
		_root.image_mc.loadMovie(_global.oMain.getBaseURL()+'api/image.php?mode='+sSize+'&imageid='+this._sImage);
		this._sCondition = 'image';
	}
	
	public function exportImage() {
		getURL(_global.oMain.getBaseURL()+'api/image.php?mode=export&imageid='+this._sImage, "_blank");
	}
	
	public function onKeyDown(oKey:Key) {
		if (this._sCondition == 'detail') {
			this._onKeyDown4detail(oKey);
		}
		if (this._sCondition == 'image') {
			this._onKeyDown4imageviewer(oKey);
		}
	}
	private function _onKeyDown4detail(oKey:Key) {
		switch(oKey.getAscii()) {
		case 50: // 2
			_root.detailBox_mc.content_txt.scroll -= 3;
			break;
		case 53: // 5
			_root.detailBox_mc.content_txt.scroll += 3;
			break;
		}
	}
	private function _onKeyDown4imageviewer(oKey:Key) {
		switch(oKey.getAscii()) {
		case 50: // 2
			_root.image_mc._y += 10;
			break;
		case 53: // 5
			_root.image_mc._y -= 10;
			break;
		case 52: // 4
			_root.image_mc._x -= 10;
			break;
		case 54: // 6
			_root.image_mc._x += 10;
			break;
		case 51: // 3
			_root.image_mc._xscale -= 5;
			_root.image_mc._yscale -= 5;
			break;
		case 57: // 9
			_root.image_mc._xscale += 5;
			_root.image_mc._yscale += 5;
			break;
		}
	}
}
