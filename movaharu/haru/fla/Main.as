﻿class Main {
	public var _mtl:MovieClip;		// メインタイムライン
	private var _bDevelop:Boolean;
	private var _sBaseURL:String;
	private var _oListItem:ListItem;
	private var _oDetailItem:DetailItem;
	private var _oComment:Comment;
	public var _sCondition:String;
	private var _iHaruid:Number;
	private var _sSessionid:String;
	private var _iPage:Number;
	private var _paramIntervalID;
	private var _sLoadingMode:String;

	/**
	 * コンストラクタ
	 */
	public function Main(mtl:MovieClip, bDevelop:Boolean) {
		this._mtl = mtl;
		this._bDevelop    = bDevelop;
		if (this._bDevelop == true) {
			this._sBaseURL= 'http://pinoxox.xsrv.jp/haru_dev/';
			this._mtl.sample_txt.text = 'This is develop mode.';
		} else {
			this._sBaseURL= 'http://pinoxox.xsrv.jp/haru/';
		}
		this._oListItem   = null;
		this._oDetailItem = null;
		this._oComment    = null;
		this._sLoadingMode= '';
		this._sCondition  = 'init';
		this._iHaruid     = this.getHaruid(_root._url);
		this._sSessionid  = this.getSessionid(_root._url);
		this._iPage       = 1;
		this._paramIntervalID = null;
		Key.addListener(this);
	}
	
	private function getHaruid(sURL:String) {
		var iStart = sURL.indexOf("?haruid=");
		var iEnd = sURL.indexOf("&sessionid=");
		if (iStart > 0) {
			var iHaruid = Number(sURL.substr(iStart + 8, iEnd - (iStart + 8)));
			trace(iHaruid);
			return iHaruid;
		}
//		return 1;
		return 347;
	}
	private function getSessionid(sURL:String) {
		var iStart = sURL.indexOf("&sessionid=");
		if (iStart > 0) {
			var sSessionid = sURL.substring(iStart + 11);
			trace(sSessionid);
			return sSessionid;
		}
		return 'pino';
	}
	
	/**
	 * 読み込みモードの設定.
	 */
	public function setLoadingMode(sLoadingMode:String) {
		this._sLoadingMode = sLoadingMode;
	}
	
	/**
	 * 読み込みモードに合ったリスト取得を行う
	 */
	public function load() {
		if (this._paramIntervalID != null) return;
		
		// ロード先URLを取得
		var sURL:String;
		if (this._sLoadingMode == 'friends') {
			sURL = this.getUserNfriendPost();
		} else if (this._sLoadingMode == 'self') {
			sURL = this.getSelfPost();
		} else if (this._sLoadingMode == 'lounge') {
			sURL = this.getLoungePost();
		}
		trace(sURL);
		// ロード開始
		this._sCondition = 'loading';
		this._mtl.null_mc.res = '';
		loadVariables(sURL, this._mtl.null_mc);
		var oMain = this;
		this._paramIntervalID = setInterval(function(){oMain.checkParamsLoaded();}, 100);
	}
	
	/**
	 * ベースになるURLを返す
	 */
	public function getBaseURL() {
		return this._sBaseURL;
	}
	
	/**
	 * 友だちとの書き込みリスト取得用URLを返す
	 */
	private function getUserNfriendPost() {
		return this.getBaseURL()+"api/index.php?mode=friends&haruid="+ this._iHaruid +"&page="+ this._iPage;
	}
	/**
	 * 自分の書き込みリスト取得用URLを返す
	 */
	private function getSelfPost() {
		return this.getBaseURL()+"api/index.php?mode=self&haruid="+ this._iHaruid +"&page="+ this._iPage;
	}
	/**
	 * ラウンジをみるリスト取得用URLを返す
	 */
	private function getLoungePost() {
		return this.getBaseURL()+"api/index.php?mode=lounge&page="+ this._iPage;
	}
	/**
	 * ロードできたかどうかのチェック
	 */
	public function checkParamsLoaded() {
		if (this._mtl.null_mc.res == '') return;
		
		clearInterval(this._paramIntervalID);
		this._paramIntervalID = null;
		
		if (this._mtl.null_mc.res == 'SUCCESS') {
			_global.oMain.getUserNfriendPostSuccess(this._mtl.null_mc);
		} else {
			_global.oMain.getUserNfriendPostFaild();
		}
	}
	/**
	 * 友だちとの書き込みリスト取得成功/失敗
	 */
	public function getUserNfriendPostSuccess(oData_mc:MovieClip) {
		this._mtl.sample_txt.text = 'res:'+oData_mc.res+',content:'+oData_mc.max_list;
		
		this._oListItem   = new ListItem(oData_mc);
		this._oDetailItem = new DetailItem(oData_mc);
		this._oComment    = new Comment(this._sSessionid);
		
		this._sCondition = 'list';
		this._mtl.gotoAndStop('list');
	}
	public function getUserNfriendPostFaild() {
		this._sCondition = 'failure';
		this._mtl.gotoAndStop('failure');
	}
	/**
	 * リスト読み込みキャンセル
	 */
	public function loadingCancel() {
		clearInterval(this._paramIntervalID);
		this._paramIntervalID = null;
	}
	
	/**
	 * リスト表示
	 */
	public function makeListing() {
		this._mtl.page_mc.page_txt.text = 'Page: '+this._iPage;
		
//		this._oListItem.makeListing();
		this._oListItem.changeList(0);
	}
	/**
	 * ページングダイアログ表示
	 */
	public function makePagingDialog(iDirection:Number) {
		this._mtl.pageDialog_mc.caption_txt.text = Number(this._iPage + iDirection) +'ページ目を表示しますか?';
	}
	/**
	 * 詳細表示
	 */
	public function makeDetailPost() {
		var iNo = this._oListItem.getListPosition();
		this._oDetailItem.makeDetailPost(iNo);
	}
	/**
	 * コメント送信
	 */
	public function submitComment(sComment:String) {
		this.addComment(sComment);
		this._oComment.submit();
	}
	/**
	 * コメント追加モード
	 */
	public function addComment(sComment:String) {
		var iNo:Number = this._oListItem.getCurrentListPositionId();
		this._oComment.addList(iNo, sComment);
		
		this._sCondition = 'detail';
		this._mtl.gotoAndStop('detail');
	}
	/**
	 * コメントのみ送信
	 */
	public function sendKome() {
		this._oComment.submit();
	}
	/**
	 * 画像表示
	 */
	public function makeImage(sSize:String) {
		this._oDetailItem.makeImage(sSize);
	}
	/**
	 * 外部サイトで画像表示
	 */
	public function exportImage() {
		this._oDetailItem.exportImage();
	}
	
	function onKeyDown() {
//		trace(Key.getCode()+","+Key.getAscii());
		switch(Key.getAscii()) {
		case 52: // 4
			if (this._sCondition == 'loading') {
				this.loadingCancel();
				this._sCondition = 'init';
				this._mtl.gotoAndStop('init');
				return; // ListItem, DetailItemには渡さない
			}
			if (this._sCondition == 'list') {
				this._sCondition = 'init';
				this._mtl.gotoAndStop('init');
				return; // ListItem, DetailItemには渡さない
			}
			if (this._sCondition == 'detail') {
				this._sCondition = 'list';
				this._mtl.gotoAndStop('list');
				return; // ListItem, DetailItemには渡さない
			}
			break;
		case 54: // 6
			if (this._sCondition == 'list') {
				this._sCondition = 'detail';
				this._mtl.gotoAndStop('detail');
				return; // ListItem, DetailItemには渡さない
			}
			if (this._sCondition == 'detail'
			&&  this._oDetailItem.hasImage()) {
				this._sCondition = 'image';
				this._mtl.gotoAndStop('image');
				return; // ListItem, DetailItemには渡さない
			}
			break;
		case 42: // *
			if (this._sCondition == 'list') {
				if (this._iPage > 1) {
					this._sCondition = 'paging';
					this._mtl.gotoAndStop('pr-paging');
					return; // ListItem, DetailItemには渡さない
				}
			}
			if (this._sCondition == 'image') {
				this._sCondition = 'detail';
				this._mtl.gotoAndStop('detail');
				return; // ListItem, DetailItemには渡さない
			}
			if (this._sCondition == 'detail') {
				this.sendKome();
				return; // ListItem, DetailItemには渡さない
			}
			break;
		case 35: // #
			if (this._sCondition == 'list') {
				this._sCondition = 'paging';
				this._mtl.gotoAndStop('nx-paging');
				return; // ListItem, DetailItemには渡さない
			}
			break;
		case 51: // 3
			if (this._sCondition == 'detail') {
				this._oListItem.changeList(-1);
				this.makeDetailPost();
				return; // ListItem, DetailItemには渡さない
			}
			break;
		case 57: // 9
			if (this._sCondition == 'detail') {
				this._oListItem.changeList(1);
				this.makeDetailPost();
				return; // ListItem, DetailItemには渡さない
			}
			break;
		case 48: // 0
			if (this._sCondition == 'detail') {
				this._sCondition = 'comment';
				this._mtl.gotoAndStop('comment');
				return; // ListItem, DetailItemには渡さない
			}
			if (this._sCondition == 'comment') {
				this._sCondition = 'detail';
				this._mtl.gotoAndStop('detail');
				return; // ListItem, DetailItemには渡さない
			}
			if (this._sCondition == 'image') {
				// 外部サイトへ遷移
				this.exportImage();
				return; // ListItem, DetailItemには渡さない
			}
			break;
		}
		
		if (this._sCondition == 'list')
			this._oListItem.onKeyDown(Key);
		if (this._sCondition == 'detail')
			this._oDetailItem.onKeyDown(Key);
		if (this._sCondition == 'image')
			this._oDetailItem.onKeyDown(Key);
	}
	// キー入力の *,# ではユーザのアクセスとは認められないらしいので、別の方法で対処
	public function prevPage() {
		if (this._iPage > 1) {
			this._iPage--;
			this._mtl.gotoAndStop('loading');
		}
	}
	public function nextPage() {
		this._iPage++;
		this._mtl.gotoAndStop('loading');
	}
	public function cancelPage() {
		this._sCondition = 'list';
		this._mtl.gotoAndStop('list');
	}
}
