﻿class Comment {
	private var _sSessionid:String;
	private var _paramIntervalID;
	private var _oCommentNull_mc:MovieClip;
	private var _aCommentList:Array;
	private var _iSendingLength:Number;

	/**
	 * コンストラクタ
	 */
	public function Comment(sSessionId:String) {
		this._sSessionid = sSessionId;
		this._aCommentList = new Array();
	}
	
	/**
	 * コメントリストに加える
	 */
	public function addList(iNo:Number, sComment:String) {
		var oCommentInfo:Object = new Object();
		oCommentInfo.iNo = iNo;
		oCommentInfo.sComment = sComment;
		oCommentInfo.iCommentId = this._makeCommentId();
		this._aCommentList.push(oCommentInfo);
		_global.oMain._mtl.kome_mc.num_txt.text = this._aCommentList.length;
	}
	private function _makeCommentId():Number {
		var oDate = new Date();
		return oDate.getTime();
	}
	
	/**
	 * 送信
	 */
	public function submit() {
		if (this._aCommentList.length <= 0) return;	// コメントリストが無ければやらない
		if (this._paramIntervalID != null) {
			// 送信中なら止める
			this._sendStop();
			return;
		}
		
		this._oCommentNull_mc = _global.oMain._mtl.kome_mc.kome_null_mc;
		
		// comment_mcをSendingに変える
		_global.oMain._mtl.kome_mc.gotoAndStop('sending');
		
		// ここ、どうやってPOSTするんだろうか??
		var sURL = _global.oMain.getBaseURL() +"api/sender.php?id="+ this._sSessionid +'&length='+ this._aCommentList.length;
		for (var i = 0 ; i < this._aCommentList.length ; i++) {
			var oCommentInfo = this._aCommentList[i];
			sURL += "&no"+ i +"="+ oCommentInfo.iNo
				 +  "&comment"+ i +"="+escape(oCommentInfo.sComment)
				 +  "&cmtid"+ i + "="+oCommentInfo.iCommentId;
		}
		trace('send comment:'+sURL);
		this._iSendingLength = this._aCommentList.length;
		
		// 送信開始
		this._oCommentNull_mc.res = '';
		loadVariables(sURL, this._oCommentNull_mc, 'GET');
		var oComment = this;
		this._paramIntervalID = setInterval(function(){oComment.checkParamsLoaded();}, 100);
	}
	private function _sendStop() {
		if (this._paramIntervalID != null) {
			clearInterval(this._paramIntervalID);
			this._paramIntervalID = null;
		}
		
		// comment_mcをdefaultに変える
		_global.oMain._mtl.kome_mc.gotoAndStop('default');
		return;
	}
	
	/**
	 * ロードできたかどうかのチェック
	 */
	public function checkParamsLoaded() {
		// ネットワークが「0:接続済」でも「1:接続中」でも「-1:未サポート」でもないなら、送信をやめる
		// ここでコレが必要なのは、送信失敗すら返ってこない場合に永遠に送信中になるため
		var iNetworkStatus = FSCommand2("GetNetworkConnectStatus");
		if (iNetworkStatus >= 2) {
			this._sendStop();
			return;
		}
		
		if (this._oCommentNull_mc.res == '') return;
		
		this._sendStop();
		
		// 失敗してたら無視
		if (this._oCommentNull_mc.res != 'SUCCESS') return;
		
		// 送信済みデータの削除と、コメ数の変更
		for (var i = 0 ; i < this._iSendingLength ; i++) {
			this._aCommentList.shift();
		}
		trace('FIX. at last '+this._aCommentList.length);
		
		_global.oMain._mtl.kome_mc.num_txt.text = this._aCommentList.length;
	}
	
	public function onKeyDown(oKey:Key) {
	}
}
