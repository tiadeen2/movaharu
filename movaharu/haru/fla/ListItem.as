﻿class ListItem {
	private var _oData_mc:MovieClip;
	private var _iListPosition:Number;
	private var _iMaxList:Number;
	private var _iScroll:Number;
	private var _iOffset:Number;

	/**
	 * コンストラクタ
	 */
	public function ListItem(oData_mc:MovieClip) {
		this._oData_mc = oData_mc;
		this._iListPosition = 0;
		this._iMaxList = this._oData_mc.max_list;
		this._iScroll  = 0;
		this._iOffset  = 0;
	}
	
	public function getListPosition():Number {
		return this._iListPosition;
	}
	public function getCurrentListPositionId():Number {
		return eval('this._oData_mc.item'+(this._iListPosition)+'_id');
	}
	
	/**
	 * 友だちとの書き込みリスト表示
	 */
	public function makeListing() {
		for (var i:Number = 0 ; i < 6 ; i++) {
			var oItemBox_mc:MovieClip = eval('itemBox'+ i +'_mc');
			
			if (this._iScroll == i)
				oItemBox_mc.gotoAndStop('on');
			else 
				oItemBox_mc.gotoAndStop('off');
			
			var aContent = {content:'',regDate:'',user:'',commentCount:0};
			aContent['content']      = eval('this._oData_mc.item'+(this._iOffset+i)+'_content');
			aContent['regDate']      = eval('this._oData_mc.item'+(this._iOffset+i)+'_regDate');
			aContent['user']         = eval('this._oData_mc.item'+(this._iOffset+i)+'_user');
			aContent['commentCount'] = eval('this._oData_mc.item'+(this._iOffset+i)+'_commentCount');
			
			oItemBox_mc.content_txt.htmlText = '<font color="#0000ff">'+aContent['user']+'</font>'+'：'+aContent['content'];
			oItemBox_mc.time_txt.htmlText    = 'at '+aContent['regDate']+' コメ数:'+aContent['commentCount'];

		}
	}
	public function onKeyDown(oKey:Key) {
		switch(oKey.getAscii()) {
		case 50: // 2
			this.changeList(-1);
			break;
		case 53: // 5
			this.changeList(1);
			break;
		}
	}
	public function changeList(iDirection:Number) {
		if (iDirection == -1) if (this._iListPosition + iDirection < 0) return;
		if (iDirection ==  1) if (this._iListPosition + iDirection > this._iMaxList-1) return;
		
		// 下(上)まで行ったらスクロール発生にしてはどうか? FF方式! (10分で実装orz)
		this._iListPosition += iDirection;
		if (this._iScroll + iDirection < 0) {
			this._iOffset--;
		} else if (this._iScroll + iDirection >= 6) {
			this._iOffset++;
		} else {
			this._iScroll += iDirection;
		}
		this.makeListing();
	}
}